# German pancakes (Dutch baby pancake)

## Ingredients

- 3 eggs
- &frac12; C flour
- &frac12; C milk
- 1 T sugar
- Pinch of nutmeg (optional)
- 4 T butter
- Syrup (for serving)

## Instructions

- Preheat oven to 425&deg;F
- In a bowl, mix all ingredients except butter and syrup
- Once the oven is preheated, put the butter in an oven-safe pan (I use a pie dish) and put it in the oven until the butter is melted
- Remove pan from oven, pour batter into the melted butter, and bake for 8&ndash;10 minutes, or until poofy and golden brown
- Remove, top with syrup or whatever, and enjoy