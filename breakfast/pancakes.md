# Pancakes

## Ingredients

- 1&frac14; C flour
- 1 T baking powder
- 1 T sugar
- 1 t salt
- 1 egg
- 1 C milk
- 2 T oil

Yields 8 four inch pancakes.

## Instructions

1. Sift together flour, baking powder, sugar, and salt.
1. Add egg, milk, and oil. Beat.
1. Bake on medium griddle (350-375&deg;F).
