# Bacon and cornmeal pancakes

## Ingredients

- 1&frac14; C flour
- &frac34; C cornmeal
- &frac14; C sugar
- 2 t baking powder
- &frac12; t salt
- 1&frac13; C milk
- &frac14; C oil
- 1 egg
- &frac12; lb bacon

## Instructions

1. Combine flour, cornmeal, sugar, baking powder, and salt.
1. Separately, mix milk, oil, and beaten egg.
1. Combine and whisk. The batter is fine a little lumpy.
1. Chop 5&ndash;6 strips of bacon and toss into frying pan. Brown. Add to completed batter.
1. Cook over medium heat until golden brown.
