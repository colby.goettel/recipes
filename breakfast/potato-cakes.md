# Potato cakes

## Ingredients

- Mashed potatoes
- 1 egg
- 1t sugar, heaping
- Pinch of salt
- Flour, enough to stiffen
- Cottage cheese

## Instructions

1. Make mashed potatoes
1. Add egg, sugar, salt, and cottage cheese. Mix.
1. Add enough flour to stiffen.
1. Cook like pancakes.
