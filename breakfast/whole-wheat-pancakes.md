# Whole wheat pancakes

From Grandma Grover

## Ingredients

- &frac34;C whole wheat
- 1C milk
- 2 eggs
- 4T oil
- 2T sugar
- &frac14;t salt
- 2t baking powder
- 1t baking soda
- about &frac14;C white flour

## Instructions

1. Blend milk and wheat for four minutes
1. Add other ingredients
1. Cook on skillet
