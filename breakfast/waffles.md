# Arlene Kovash's morning waffles

## Ingredients

- 1&frac34; C flour
- 1 T baking powder
- &frac12; t salt
- 1&frac14; C milk
- &frac12; C oil
- 2 eggs
- (Optional) 1&frac12; t vanilla

## Instructions

1. Preheat waffle iron.
1. Separate 2 eggs and place the egg whites in a bowl so you can beat them.
1. Mix the 2 egg yolks, milk, and oil. Optionally, add vanilla. Add flour, baking powder, and salt.
1. Beat the egg whites until fluffy. Fold into the batter.
1. Cook.

## Notes

- These are the fluffiest waffles ever. However, feel free to not beat the egg whites and just combine everything at once. If I'm just making these for myself, that's all I do and they're great.
