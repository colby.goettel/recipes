# Granola

## Ingredients

- 3 C old fashioned oats
- 1 C slivered or sliced almonds
- 1 C coconut
- 3 T flour
- 1 t cinnamon
- &frac18; t salt
- &frac12; C maple syrup
- &frac13; C coconut oil
- 1 t almond extract

## Instructions

1. Preheat oven to 300&deg;F.
1. Mix together dry ingredients.
1. In a separate bowl, whisk together maple syrup, coconut oil, and almond extract.
1. Add liquid mixture to dry ingredients and stir until evenly coated.
1. Spread mixture onto a parchment-lined baking sheet.
1. Bake 40&ndash;50 minutes, stirring halfway through.
1. Let cool and store in an airtight container.

## Notes

- Looking at the ingredients, I thought that vanilla extract would be a good addition. Didn't really do anything positive and I prefer this without any vanilla.
- The original recipe had the salt and cinnamon added to the wet ingredients. It's way harder to evenly coat if you do it that way.
