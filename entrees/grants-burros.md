# Grant's burros

From Grant Massey

## Ingredients

- 2&ndash;3 lb chuck roast, shredded
- 2 lbs ground beef
- 1 yellow onion, chopped
- Salt and pepper, to taste
- 1&ndash;2 T flour
- 16 oz beef broth
- 16 oz tomato sauce
- 1 can green chiles (short or tall, depends how you like it; I prefer more)
- 7&ndash;9 T chili powder, to taste (see Notes)

## Directions

- Slow cook [chuck roast](chuck-roast.md). Shred. Set aside.
- Separately, cook ground beef and onion. Season to taste with salt and pepper.
- Add flour and cook down to remove flour flavor.
- Add beef broth, tomato sauce, and green chili.
- Bring to boil, reduce heat, and let thicken.
- Add shredded beef.
- Season to taste with chili powder (see Notes).

## Notes

- I measured out the chili powder the most recent time and ended on 8 T. It's really going to depend on how you season the chuck roast and how large the roast is. Everything else is pretty evenly measured and repeatable. The chuck roast is your real variable.
