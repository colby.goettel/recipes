# Lasagna

## Ingredients

- 1 batch of (red sauce)[../sauces/red-sauce.md]
- béchamel:
  - 2 T butter
  - 2 T flour
  - 2 C whole milk
  - salt, to taste
  - freshly grated nutmeg, to taste
- spinach
- Parmigiano reggiano, finely grated
- Low moisture mozzarella
- 1 box oven-ready lasagna noodles

## Steps

- Make a batch of red sauce
- Make the béchamel and season to taste
- Chop and bunch of spinach and add it to the béchamel. Add as much as you can. You'll be surprised how much you can get away with here. And don't worry about cooking the spinach and squeezing it dry or anything. Just add it in raw.
- Preheat oven to 400&deg;F
- Grab a lasagna pan. Lightly coat the bottom and sides with the spinach béchamel.
- Layer lasagna noodles
- Alternate layers of béchamel/parmigiano reggiano and red sauce/mozzarella
- Cover top with mozzarella and parmigiano reggiano
- Tent foil over top, bake for 40 minutes
- Remove foil, bake for an additional 15&ndash;20 minutes until cheese is toasty
- Remove from oven and allow to rest for 40 minutes before cutting into it. Do not skip this step or you'll have watery lasagna.

## Notes

- Fresh mozzarella, while incredible, has too much moisture for this recipe and it tends to come out runny. No good.
