# Broccoli and cheese soup

## Ingredients

- 1 T olive oil
- 4 T butter
- 1 medium onion
- 1&ndash;2 stalks celery
- 2 large carrots
- 2 stalks broccoli (maybe more depending on tastes)
- 1 head cauliflower
- 2 C chicken stock (or vegetable stock if you prefer)
- 2 C heavy cream (use half and half for a lighter soup)
- 1&frac12; T tomato paste
- Salt
- Black pepper
- Paprika
- All of the cheese

## Directions

- In a large, heavy-bottomed put, melt the butter in olive oil until it begins to foam.
- Sautée the onions, celery, and carrots in butter until aromatic.
- While that's cooking, cut up your broccoli and cauliflower. This is a place where you can get creative. Maybe throw in some rutabaga or parsnip. Potatoes would be great, but this can easily be a keto recipe if you control your vegetables.
- When the onions, celery, and carrots are aromatic, add the tomato paste and cook for a minute. This will add depth to the dish.
- Slowly pour in the stock.
- Slowly pour in the cream and reduce heat to a simmer.
- Add the broccoli and cauliflower. I like to use enough that the soup is at peak veggie concentration.
- Simmer over low heat for 15&ndash;20 minutes until the vegetables are cooked how you like.
- Season. This is another place you can get creative. I really like to add just a little garam masala (adds some great depth) and some crushed chilis.
- Start adding cheese. Add all of the cheese that you want. I add at least a pound. It's crazy thick and a small bowl will fill you up.
