# Birria de res

This recipe is from [La Capital](https://www.youtube.com/watch?v=g2F5RO6vNSs).

## Ingredients

- Oil to coat bottom of pot
- 10 Roma tomatoes, quartered
- 1 white onion, quartered
- 10 cloves garlic
- 9 g whole allspice
- 8 g whole black peppercorn (looks like 1&ndash;2 T each)
- 1 t cumin, heaping
- 1&frac12; t dried thyme
- 1&frac12; t dried oregano
- Little less than half of a stick of dried cinnamon (the big kind you find at a Mexican market) (I used 4")
- 3 bay leaves
- 5 chile ancho, devein and remove seeds
- 8 chile guajillo, devein and remove seeds (see Notes)
- 2 T white vinegar
- 1 L water

### Braising liquid

- 2 L water
- 4 beef short ribs (see Notes)
- Salt to taste (see Notes)

### Garnish

- Cilantro, chopped
- White onion, diced
- Radish, sliced thinly or diced

## Instructions

1. In a large (8+ qt) heavy-bottom pot, add oil, tomatoes, onion, and garlic.
1. Sweat for 10&ndash;15 minutes. There should be a decent layer of moisture at the bottom.
1. Add the allspice, black peppercorn, cumin, thyme, oregano, cinnamon, bay leaves, and chiles.
1. Add the white vinegar and water and bring to a boil.
1. Lower the tempurature, partially cover, and vigorously simmer for 5&ndash;10 minutes, until the chiles are rehydrated and soft.
1. Transfer mixture to a blender and blend until smooth. Don't hesitate to blend this in batches, it's a lot.
1. In a heavy-bottom pot, add 2 L water and and the blended mixture. Salt to taste (see Notes).
1. Bring to a boil, add the meat, and vigorously simmer until the meat falls off the bone (5+ hours).
1. Shred the meat and chop it so you don't have any long strands of meat.
1. Filter the consomé (broth) through a fine mesh sieve.
1. Next, you need to separate the fat and set it aside. Either chill overnight in the fridge and remove the fat, or use a fat separator.
1. Taste the broth and season to taste. When I make this, the broth is _very rich_ and oversalted, so I end up watering it down. This is because I only have an 8 qt pot and it's filled to the brim when cooking.
1. Add meat and broth to a bowl and garnish with cilantro, diced white onions, and radish, to taste.
1. Serve with corn tortillas. Alternatively, you can add ramen noodles to the dish to make birria ramen.

## Notes

- If you can't find chile guajillo, chile California is a perfectly fine substitute.
- When I made this, I used 1725 g short ribs. Accounting for the bones, that's probably only a half pound of meat. You could easily use a chuck roast for this recipe and be just happy. I don't know if the marrow really brought anything to it. I didn't boil long enough to gelatinize the broth.
- Be careful when salting this. But don't be afraid to water down the consomé because it's probably too rich at the end anyway. These are the main issues that you will run into that directly affect the amount of salt you need to use:
  - You're not going to lose much water because the lid will be on tight.
  - Once the meat is tender, you will filter out the broth which removes a significant amount of biomass. It's really easy to oversalt because you're adding salt when all of this is still in the dish.
  - You'll be adding a lot of meat which will need the salt, but it's easy to over do because of the aforementioned excess biomass.
