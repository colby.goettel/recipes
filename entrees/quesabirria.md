# Quesabirria

This recipe is from [La Capital](https://www.youtube.com/watch?v=g2F5RO6vNSs). He doesn't provide a recipe, so I've (mostly) translated what he did. I will be making these soon and editing the recipes (this, birria de res, and salsa birria) as I go.

## Ingredients

- 1 recipe [birria de res](birria-de-res.md)
- Corn tortillas
- Oaxaca cheese, grated (see Notes)

### Garnish

- Lime wedges
- Radish, sliced or diced
- White onion, diced
- Cilantro, chopped

## Instructions

1. Make birria de res and save the fat that you skim off.
1. Make sure the consomé is hot and ready to serve.
1. Heat a large griddle and use the birria fat to coat the surface. For each taco, dunk the tortilla in consomé and place on the griddle. Add meat and cheese. Fold it over and allow it to crisp up. Flip and allow to crisp.
1. Serve with lime, onion, cilantro, and consomé (garnished with onion, cilantro, and radish).

## Notes

- Any white melting cheese will be perfect here. I went to the local carnicería, told them I was making quesabirria, and the lady recommended some white cheese. I'll have to go back and get the name because it was perfect.
- These are so flavorful and the consomé so rich that I never use any salsa. You do you.
