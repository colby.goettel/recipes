# Meatballs

This recipe is inspired by [Matty Matheson's meatball recipe](https://www.vice.com/en/article/8x5wq3/spaghetti-and-meatballs-recipe) and [Walter Food's meatball subs recipe](https://www.vice.com/en/article/3d4gvn/meatball-subs).

## Ingredients

- 2 pounds meat (beef, pork, veal, or combo of two or all three)
- 2 T crushed red pepper flakes
- 3 T dried oregano
- 2 T fennel seeds (optional)
- 1 T celery seeds
- 2 C panko
- &frac12; bunch Italian parsley, chopped (optional)
- 1 C ricotta cheese
- 1 C freshly grated parmigiano reggiano
- 3 large eggs
- &frac12; C whole milk
- 2&frac12; T kosher salt
- fresh-cracked pepper, to taste

Yields 24 2&frac12;" meatballs

## Steps

- Preheat oven to 425&deg;F
- Put all ingredients into a large bowl
- Get in there and mix it up. Hands work best.
- Roll into balls (however large or small you want) and place on baking sheet
- Bake for 20 minutes
- I only make these if I'm also making a red sauce, so at this point I transfer the meatballs to the red sauce to let them braise until cooked. They usually get added to the red sauce about thirty minutes into the reduction.

## Notes

- This recipe makes 24 _big_ meatballs. That's way too much for one or two people. Halve the recipe unless you're feeding a family.
