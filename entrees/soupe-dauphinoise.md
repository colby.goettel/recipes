# Soupe Dauphinoise

This recipe is adapted from the [French Cooking Academy](https://www.thefrenchcookingacademy.com/recipes/dauphinoise-soup). I adjusted the amounts just a little and added a couple ingredients.

## Ingredients

- 1 T olive oil
- 50 g (1.76 oz) butter
- 2 onions
- 2 carrots
- 1 leek
- 1 15.5 oz can white (Navy) beans
- 3 T chopped canned tomatoes (or 2 chopped tomatoes if ripe and in season)
- 1 L (32 oz) beef broth
- 3 T heavy cream or crème fraîche
- 2 T chopped parsley (or chives)
- Thyme, to taste
- Salt and pepper, to taste
- Toasted bread to serve

## Method

1. Thinly slice the leeks, onions, and carrots.
1. Warm the olive oil and melt the butter in a stock pot on medium heat until the butter starts to foam.
1. Stir in the leek, onions, and carrot. Cook gently for 15 minutes with the lid partly on, stirring occasionally until cooked. Take care not to allow any colouration of the vegetables.
1. Drain the beans.
1. Pour in the stock and mix in the beans and tomatoes. Add thyme and pepper, to taste.
1. Bring to a simmer and then lower the heat and cook gently for 30 minutes, partly covered.
1. Once done, mix in the cream and parsley (or chives).
1. Cook for a further five minutes and salt to taste. 
1. Enjoy with toasted crusty bread.
