# Chuck roast

## Ingredients

- 2&ndash;3 lb chuck roast
- 1 yellow onion, quartered
- &frac14; C soy sauce
- chili powder
- pepper
- 1&ndash;1&frac12; C water

## Directions

- Sear chuck roast and place in crock pot.
- Pour soy sauce over roast. Add enough pepper to light&ndash;medium coat the roast.
- Coat with chili powder.
- Add water around, but not over, roast.
- Add onions to the liquid.
- Cook slow and low for several hours, flipping occasionally. It's done with you can insert a fork and twist it apart without much effort.
