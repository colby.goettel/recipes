# Grandma Goettel's enchiladas

From [Grandma Goettel](https://www.familysearch.org/tree/person/details/KWCR-R7H), but Mom has adapted it.

## Ingredients

- Corn tortillas
- Oil (for frying)
- Ground beef (1&ndash;2 lbs, depending on how many people you're feeding)
- Salt and pepper, to taste
- Cheese, shredded (we use cheddar or a Mexican cheese blend)
- 1 onion, diced
- 1 head iceberg lettuce, shredded

### Sauce

- 1 46 oz can tomato juice
- &frac34; tomato juice can water
- 1 can El Pato (yellow can, not the green one)
- 1 T garlic powder
- 1 T kosher salt
- 2-3 T chili powder
- &frac34; t pepper
- 2 C cold water
- 1 C flour

## Instructions

1. Make the sauce:
  1. Pour tomato juice into a large saucepan.
  1. Refill can &frac34; full of water and pour into saucepan.
  1. Add El Pato. For a mild sauce, add &frac14;-&frac12; can. For a fuller flavor, add the whole can. It's really not that spicy.
  1. Add garlic powder, kosher salt, chili powder, and pepper.
  1. Bring to a boil.
  1. Separately, whisk 1 C flour into 2 C cold water.
  1. Pour flour solution through a sieve into the saucepan and whisk until sauce is at desired thickness.
1. Prepare the beef. Season with Lawry's season salt and pepper.
1. In a separate pan, heat the oil. When you think it's ready, tear off a piece of a corn tortilla (like &frac18;"x&frac18;") and drop it in the oil. It should immediately bounce back to the surface. If it doesn't keep heating.
1. Dice the onion and shred the lettuce. Set aside.
1. These are stacked enchiladas. It's easiest to assemble with two people working in tandem with one person frying and the other person assembling. Make sure to read the notes before continuing. To make them:
  1. Fry a tortilla. It should only take 5 or so seconds on each side. They should still be soft.
  1. Put the tortilla on the plate. Ladle a small amount of sauce onto the tortilla and spread it around evenly.
  1. Add a spoonful of meat onto the sauce and distribute evenly.
  1. Add cheese, as much as you want.
  1. I like to add onions on every layer. You do you.
  1. Repeat for each layer desired. Typically people have 3&ndash;5.

## Notes

- I've been making these for a long time and have learned a few tips and tricks. I hope they help:
  - Figure out how many enchiladas someone wants before you start assembling. Count out that many tortillas and set them aside. It makes it a lot easier to keep track.
  - Go easy on the sauce. You can always add more at the end.
  - This is going to sound weird, but don't grip the plate by the edge. Put your hand under it. If you're making these for an army, your wrist will thank me.
- A great way to make these go further is to fry an extra tortilla and use it to mop up sauce. Works great for kids.
