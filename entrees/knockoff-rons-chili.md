# Knockoff Ron's chili recipe

## Ingredients

### Seasoning

- &frac12; C chili powder
- 1 T oregano
- 1 T cumin
- 1 T paprika
- 1 T black pepper
- 1 T salt
- 2 t granulated garlic

### Cooking the meat

- 1&ndash;1&frac12; C water
- 2 lbs ground beef
- &frac12; C flour

## Directions

- Combine seasonings in a bowl. Set aside.
- Bring water to a boil (or heavy simmer) in a large saucepan.
- Add beef and cook. Mince as finely as possible.
- Once beef is no longer pink, add seasonings and stir them in.
- Continue cooking until most of the liquid is gone (about 10&ndash;15 minutes).
- Add flour and stir to incorporate it all.
- Add water until desired consistency is reached.
- Submersion blend.
- Bring to boil and remove from heat. Let it sit a while to thicken.
