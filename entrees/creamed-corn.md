# Creamed corn

## Ingredients

- 3 C frozen corn
- &frac12; C whole milk
- 4 oz cream cheese
- &frac14; C butter
- 1&frac12; t sugar
- &frac12; t pepper
- &frac12; t salt

## Directions

- Throw everything into a crock pot. Stir until the cream cheese has melted in thoroughly.
