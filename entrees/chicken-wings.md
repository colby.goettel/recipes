# Chicken wings

## Ingredients

### Brine

- 6 C water
- &frac12; C kosher salt
- <sup>1</sup>&frasl;<sub>3</sub> C sugar
- &frac14; C white vinegar
- &frac14; C buffalo sauce
- 2 T black pepper
- 3 lbs chicken wings

### Frying

- Lard. Lots of lard.

## Directions

- Combine all brine ingredients. Let chicken brine for up to 24 hours. I like to do the brine the day before and fry the next day.
- Remove brine from fridge and place on stovetop. Bring to boil, turn off heat, and let sit for 10 minutes. This will cook the wings all the way through so you don't have to worry about the chicken's internal temperature while frying.
- Melt a bunch of lard in a large, high-sided saucepan. Fry wings until golden brown.
