# Masa

## Ingredients

- 2 C corn flour
- 2 C broth
- 1 t baking powder
- &frac12; t salt
- &frac23; C lard or shortening

## Directions

- Mix everything together.
