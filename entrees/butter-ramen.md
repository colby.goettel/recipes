# Butter ramen

I got this recipe off of Instagram and have adapted it to my tastes and methods.

## Ingredients

- 1 pack ramen (Ichiban is my preferred brand; any flavor you like; I prefer tonkatsu)
- 1 T butter
- Diced veggies
- &frac12; C milk (I prefer coconut milk, but cow milk works just fine)
- Cracked pepper to taste

## Instructions

- In a small pot, get some water boiling (enough for your noodles). Once it's boiling, add the noodles (NOT the seasoning packet), boil for 2 minutes, drain, and set aside.
- Separately in a pan, melt the butter and cook your veggies. You have total freedom here. Onion, mushrooms, zucchini, etc.
- Once the veggies are cooked, add the milk and seasoning packet, and stir to incorporate.
- Add the drained noodles, and simmer and stir until the noodles absorb all of the milk. Season with black pepper. You should have a creamy sauce evely coating the noodles.
- Dish and serve. In the original recipe, the creator garnished with a slice of American cheese and a soft-boiled egg. I don't stock American cheese and I hate peeling soft-boiled eggs. I've done it with scrambled eggs and it's great. I've used shredded cheese and it's also delicious. Get creative and enjoy!
