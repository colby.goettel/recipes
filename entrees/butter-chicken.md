# Butter chicken

Adapted from [Matty Matheson's recipe](https://www.vice.com/en/article/nede8q/simple-butter-chicken-recipe).

## Ingredients

- 6 T olive oil
- 5 garlic cloves
- 2 birds eye chiles (see Notes)
- 2 long red chiles (see Notes)
- 2 medium yellow onions, roughly chopped
- 3 thumbs worth ginger, peeled
- &frac12; C ghee
- 3 T tomato paste
- 3 T turmeric
- 2 T chili powder
- 2 T garam masala
- 2 T ground coriander
- 2 T ground cumin
- 1&frac12; C chicken stock
- 3&frac12; C tomato puree
- 4 boneless and skinless chicken breasts, cut into half inch chunks (about 1 kilogram)
- 2 C heavy cream
- 8 T butter
- fresh cilantro (optional)
- cooked rice, to serve

## Directions

- Place the olive oil, garlic, chilies, onion, and ginger in a blender and puree until smooth.
- Heat ghee in a large dutch oven over medium-high. Add the onion puree and cook until the mixture darkens slightly and softens, about 15 minutes. Stir constantly because it will pop all over the place.
- Add the tomato paste, turmeric, chili powder, garam masala, coriander, and cumin and cook for 5 minutes, or until dark and sticky.
- Add in 1&frac12; C chicken stock (water is fine, just less flavorable). Using a wooden spoon, scrape up any browned bits at the bottom of the pan.
- Stir in the tomato puree and increase the heat to high. Bring to a boil, then reduce the heat to maintain a simmer. Cover and cook, stirring occasionally, until thick, about 1 hour. Add the chicken and cook until the chicken is cooked through, about 15 minutes more.
- Add the cream and butter and stir to combine. Season with salt and serve garnished with fresh cilantro with steamed rice.

## Notes

- For the chilies, feel free to use a milder chile like a yellow chile, Anaheim, or jalapeño.
