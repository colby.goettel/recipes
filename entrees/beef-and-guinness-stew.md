# Beef and Guinness Stew

This is adapted from a recipe on TikTok from @lelo_lemons.

## Ingredients

- 650&ndash;700 g steak pieces (stewing meat)
- 2 T butter
- 1 large white onion, thinly sliced
- 2 leeks, thinly sliced
- 4 carrots, peeled and chunky sliced
- 300 g mushrooms, diced
- Any additional vegetables you want (see [Notes](#notes))
- 2 heaping T flour
- 1 pint Guinness
- 2 T tomato purée
- Salt and pepper to taste
- 1 t granulated garlic or 2 cloves fresh garlic
- 1 t thyme (or a couple of sprigs of fresh thyme)
- Condensed beef stock
- 1 T Worcestershire sauce
- 2 bay leaves
- Water to cover ingredients
- Fresh parsley (minced) to garnish (optional)

## Directions

1. If meat is not already in bite-sized pieces, cut into pieces.
1. Salt and pepper meat and, optionally, allow to dry brine in the fridge.
1. Brown meat and set it and its juices aside. We're not trying to fully cook it at this stage.
1. Add butter, leeks, and onions to pan. Sauté until browned.
1. Add mushrooms and brown.
1. Add tomato purée and cook until fragrant.
1. Add flour and mix until combined.
1. Add Guinness and granulated garlic (alternatively, substitute with fresh garlic).
1. Add bay leaves, beef stock, and Worcestershire sauce and enough water to cover ingredients.
1. Bring to a boil then reduce heat, cover, and allow to simmer for 2&nbsp;hours, stirring every 30&nbsp;minutes.
1. Serve with mashed potatoes and, optionally, fresh parsley.

## Notes

- First time I made this, I cut the mushrooms in quarters (which is how I do Coq à la bière) and didn't care for the big pieces. I would do smaller pieces of mushroom. It's great flavor, but not great texture in a stew.
- Using a condensed beef stock for flavor would be really helpful because you really don't want a lot of liquid. This should come out rather thick. I had to remove liquid and add a beurre manié to thicken it.
- The mashed potatoes can be spiced up with steamed cabbage or sauerkraut.
- Wish this had more vegetables. Turnips would go really well in it. It was just heavy and more veggies would help lighten it.
