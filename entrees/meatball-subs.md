# Meatball subs

## Ingredients

- Half recipe [meatballs](meatballs.md)
- 1 batch [red sauce](../sauces/red-sauce.md)
- Fresh mozzarella, sliced
- Parmigiano reggiano, finely grated
- 1 loaf French bread
- Butter

## Steps

- Set oven to broil on high
- Slice the bread in half, butter, add parmigiano reggiano and mozzarella to taste (more is best)
- Broil until bread is toasty and cheese is melty
- Slice meatballs (easier to handle), add to sandwich
- Top with desired amount of red sauce
