# Divinity

From [Grandma Goettel](https://www.familysearch.org/tree/person/details/KWCR-R7H)

## Ingredients

- 1C white Karo syrup
- 4C sugar
- 2C water
- 2 egg whites, beaten to stiff peaks
- (optional) nuts, coconut, candied fruit, or flavorings

## Instructions

1. In a saucepan, combine karo syrup, sugar, and water and cook without stirring to 260&deg;F (sea level).
1. Slowly add beaten egg whites.
1. Optionally, add nuts, coconut, candied fruit, or flavorings.
1. Beat until it holds shape.
1. Drop by tea spoons onto buttered wax paper and allow to cool.
