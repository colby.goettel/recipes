# Kay's fudge

This recipe is from a tombstone in Utah.

## Ingredients

- 2 oz chocolate
- 2 T butter
- 1 C milk
- 3 C sugar
- 1 T vanilla
- pinch salt

## Instructions

- Get your mise en place; this is essential.
- Melt butter and chocolate on low heat.
- Once melted, stir in milk and bring to a boil.
- Add sugar, vanilla, and salt. Stir to incorporate. Once the mixture reaches around 190&deg;F, stop stirring. If you keep stirring, sugar crystals will form and the fudge will be gross.
- Cook to softball stage (234&ndash;240&deg;F).
- Pour on a marble slab and let cool to 110&deg;F.
- Once cooled, beat the fudge until it is no longer glossy, shape it, and let it set and cool completely.
- Cut into pieces.
- Fudge can be stored in an airtight container on the counter for one week.
