# Pecan rolls

From [Aunt Edwina](https://www.familysearch.org/tree/person/details/KWZQ-VN9) (Uncle Verl's wife)

## Ingredients

- 6C sugar
- 1pt white Karo syrup
- 1&frac12;C canned milk
- 1t salt
- &frac13;C butter
- 1t vanilla
- 2lb pecans

## Instructions

1. Cook to hard, firm ball and beat.
1. Roll in nuts.
