# Mom's apple pie filling

## Ingredients

- 5&ndash;6 Granny Smith apples
- 2&ndash;3 T flour
- &frac34; C sugar
- cinnamon
- nutmeg
- salt

## Instructions

- Peel and cut apples into medium-sized chunks.
- Add enough flour to lightly coat apples.
- Add sugar and spices to taste.
