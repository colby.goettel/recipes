# Crumb topping

## Ingredients

- 1 C sifted flour
- &frac34; C sugar
- &frac12; C butter, softened

## Instructions

- Combine flour and sugar.
- Cut in butter with pastry knife.
