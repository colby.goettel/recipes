# Hawaiian butter mochi (mochi blondies)

This recipe is from [SBS](https://www.sbs.com.au/food/article/2022/07/15/hawaiian-butter-mochi-truly-underrated-dessert).

## Ingredients

- 3 C glutinous rice flour
- 2 C sugar
- 1 stick butter, melted
- 3 eggs
- 2 C coconut milk
- 2 t baking powder
- &frac12; t salt

## Instructions

1. Preheat oven to 350&deg;F.
1. Combine all ingredients in a bowl and whisk until smooth.
1. Pour into a lightly greased 13x9 pan.
1. Bake at 350&deg;F for 45&ndash;60 minutes, or until golden brown and cooked through.
