# See's fudge

From Aunt Diane

## Ingredients

- &frac14;lb butter
- 4&frac12;C sugar
- 1 15oz can evaporated milk
- 3 pkg (12oz) chocolate chips
- 1 8oz jar marshmallow creme
- 2t vanilla
- 2C chopped nuts

## Instructions

1. In a large bowl, mix together the chocolate chips, marshmallow creme, vanilla, and nuts.
1. Cook the butter, sugar, and evaporated milk over a low flame for 10 minutes after it reaches a roiling boil. Be sure to time a full 10 minute boil.
1. Pour into the large bowl and blend thoroughly with a wooden spoon.
1. Pour into a buttered 9x13 pan or cookie sheet and let set.
1. Cut into pieces and serve.
