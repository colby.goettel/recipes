# Tempura dipping sauce

## Ingredients

- 100 mL water with 1 t fish sauce, or 100 mL dashi broth
- 1 T soy sauce (recommend [this](https://www.amazon.com/dp/B08BKKV4TV) sauce)
- 1 T mirin
- 1 t sugar

# Directions

- Mix together all ingredients. Might have to heat it a bit to get the sugar to dissolve.

## Notes

- For katsudon, I use 100-200 mL of water to start out. I like a little more broth because I use more rice.
