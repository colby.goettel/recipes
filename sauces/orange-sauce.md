# Orange sauce

From [Grandma Goettel](https://www.familysearch.org/tree/person/details/KWCR-R7H). Fun note: this was on "Flora Goettel" letterhead (from Carl Sandberg Realty) and done in Dad's handwriting.

## Ingredients

- &frac12;C sugar
- 1T corn starch
- &frac12;t salt
- 1T butter
- 1C orange juice

## Instructions

1. Combine sugar, corn starch, and salt in a saucepan. Stir in orange juice until it thickens, stirring constantly.
1. Turn heat to low and simmer for 2 minutes.
1. Remove from heat and add butter.

## Notes

- We always had this with ham when I was a kid.
