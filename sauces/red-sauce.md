# Red sauce

## Ingredients

- 1 lb mild Italian sausage or a half recipe of [meatballs](../entrees/meatballs.md)
- olive oil
- 1 large carrot, diced
- &frac12; large onion or 1 medium onion, diced
- 1 stalk celery, diced
- 4 t minced garlic
- 2 T tomato paste
- 2 28 oz cans Cento brand whole peeled San Marzano tomatoes
- 2 C chicken stock
- 2 bunches fresh basil
- 1 parmigiano reggiano rind
- oregano, to taste
- fresh-cracked pepper, to taste
- salt, to taste (only at end of cooking)

## Instructions

1. In a large, heavy bottom pot (I prefer an enameled cast iron), cook Italian sausage. Feel free to let lots of fond develop. Once cooked, remove and set aside. Or, make meatballs and set aside.
1. Add enough olive oil to coat the bottom, add the mirepoix, season with salt, and cook until the onions are translucent and aromatic
1. Add garlic and cook until aromatic
1. Add tomato paste and cook a little to help caramelize it
1. Pour in two cans of San Marzano tomatoes. Rinse the cans with chicken stock and add that in. Use your spoon to break the tomatoes into the size chunks that you like.
1. Add 1 bunch of fresh basil, a parmigiano reggiano rind, oregano, and fresh-cracked pepper. DO NOT ADD SALT NOW.
1. Add Italian sausage or meatballs to sauce
1. Partially cover and simmer on low heat until reduced by half, stirring every 30 minutes. Make sure nothing sticks to the bottom and scrape the dried tomato paste off the sides and add it back in.
1. Once reduced by half, kill the heat.
1. Chop the other bunch of fresh basil and add it in. Add a couple glugs of a good quality olive oil. Salt to taste.
1. Feel free to immersion blend to desired consistency.

## Notes

- Don't cut corners on this recipe. You can cheap out on some ingredients, but fresh will always be best and provide the deepest flavor
- Everything that we do in this recipe is meant to deepen the flavors. You can use water instead of chicken stock, you can omit the tomato paste, you can skimp on garlic, you can skip the cheese rind, etc., but each of these steps serves to deepen and enrich the flavor and the sauce will be incrementally worse for each step you skip.
- The Cento brand is my absolute favorite. You do you.
