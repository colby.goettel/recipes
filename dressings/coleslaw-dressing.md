# Coleslaw dressing

From Uncle Bryan

## Ingredients

- 1:1 vinegar and sugar, about &frac14;C
- 1t celery seed
- Miracle whip

## Instructions

1. Combine. The recipe only specifies to "add mir. whip to con. of salad dressing"
