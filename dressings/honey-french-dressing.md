# Honey French dressing

From [Aunt Kay](https://www.familysearch.org/tree/person/details/K26F-HCV)

## Ingredients

- &frac13;C honey
- &frac13;C oil
- &frac13;C vinegar
- &frac13;C ketchup
- &frac12;t salt
- &frac12;t pepper
- &frac12;t celery seed
- &frac12;t paprika

## Instructions

1. Combine in a blender. Blend.
1. Serve over mixed greens of fruits.
