# Japanese iced coffee

From [Black Tie Kitchen](https://www.youtube.com/watch?v=AdR5wZVH-H4&t=157) on YouTube

## Ingredients

- 30 g coffee beans, finely ground (similar to kosher salt)
- 250 g ice
- Kettle set to 96&deg;C (205&deg;F)

## Instructions

1. Put the kettle on to 96&deg;C (205&deg;F)
1. Grind the coffee beans. You will want a grind that is similar in size to kosher salt.
1. Pre-wet the filter
1. Add ice to the decanter
1. Put the decanter and pour over on the scale and tare
1. Add the coffee grounds
1. Begin by pouring in twice the amount of water as coffee grounds (30&nbsp;g * 2 = 60&nbsp;g water)
1. Allow to bloom for 45 seconds
1. _Slowly_ add water until 275 g have been added
1. Swirl to evenly cool
1. Ice and enjoy
