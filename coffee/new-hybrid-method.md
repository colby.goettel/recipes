# New hybrid method

From [Tetsu Kasuya](https://www.youtube.com/watch?v=4FeUp_zNiiY).

## Ratios

- 1:15 &rarr; 20 g coffee : 300 mL water

## Instructions

Using a Hario Switch:

1. Heat water to 90&deg;C
1. Grind coffee to [865&micro;](https://onyxcoffeelab.com/pages/grind-size-calculator)
1. Close switch
1. Pour 40&ndash;50 g water, or enough to just soak grounds.
1. 0:40: Open switch and pour to 120 g
1. 1:30: Pour to 200 g
1. Lower water temperature to 70&ndash;80&deg;C
1. 2:10: close switch and pour to 300 g
1. 2:45: open switch
1. 3:30: target finish
