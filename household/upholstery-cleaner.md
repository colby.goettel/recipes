# Upholstery cleaner

1. Mix a national brand, general-purpose detergent in water and whipe the solution to a froth.
1. Skim off the bubbles (do not use the liquid) and apply uniformly to the fabric.
1. Since the froth is damp, but not wet, it can be wiped off without getting the upholstery overly wet.
