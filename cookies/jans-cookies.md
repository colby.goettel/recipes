# Jan's cookies

From Aunt Jan

## Ingredients

- 1C margarine, soft
- 1C brown sugar
- 1&frac12;C sugar
- 1T milk
- 1&frac12;t vanilla
- 2 eggs
- 1&frac12;C flour
- 1&frac14;t baking soda
- 1t salt
- &frac12;t mace
- 1&frac12;t cinnamon
- &frac14;t nutmeg
- &frac18;t powdered cloves
- 1C corn flakes
- 3C oats
- 4oz coconut
- 1pkg chocolate chips
- 1C nuts

## Instructions

1. Cream margarine, brown sugar, sugar, milk, vanilla, and eggs until smooth
1. Mix rest of the ingredients in
1. Bake at 350&deg;F for 10 minutes.
