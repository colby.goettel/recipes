# Molasses cookies

## Ingredients

- 2 C flour
- &frac12; t ground cloves
- &frac12; t ground ginger
- 2 t baking soda
- 1 t ground cinnamon
- &frac12; t kosher salt
- &frac34; C vegetable oil
- 1 C granulated sugar, plus extra for rolling dough balls in
- &frac14; molasses
- 1 large egg

## Instructions

1. Whisk together dry ingredients (flour, ground cloves, ground ginger, baking soda, ground cinnamon, and salt) and set aside.
1. In a separate bowl, mix vegetable oil, granulated sugar, molasses, and egg.
1. On low speed, mix in dry ingredients until combined.
1. Cover and chill for two hours.
    1. Not sure how required this is because the dough was pretty workable for me as is. Will try without chilling and report back.
1. Preheat oven to 375&deg;F.
1. Form 1"&nbsp;balls out of dough. Roll in granulated sugar. A larger crystal sugar is great for this.
1. Place on baking sheet and bake cookies for 8&ndash;10&nbsp;minutes at 375&nbsp;F.
1. Let cool for a couple minutes and transfer to a wire rack.
