# Oatmeal, walnut, chocolate chip cookies

Recipe adapted from Deann Ekstrom's oatmeal, walnut, and raisin cookies recipe.

## Ingredients

- 1 C butter
- 1 C brown sugar
- 1 C white sugar
- 3 eggs
- 1 t vanilla
- 2 &frac12; C flour
- 1 t salt
- 1 t cinnamon
- 2 t baking soda
- 2 C oats
- 1 C coarsely chopped walnuts
- 1 C chocolate chips

## Instructions

- Preheat oven to 350&deg;F.
- Cream together butter and sugars until fluffy.
- Add eggs and vanilla.
- Separately, mix together the flour, salt, cinnamon, and baking soda.
- Slowly spoon in the flour mixture.
- Finally, add oats, walnuts, and chocolate chips.
- Bake at 350&deg;F for 10&ndash;12 minutes.
