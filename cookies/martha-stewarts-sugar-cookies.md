# Martha Stewart's sugar cookies

## Ingredients

- 4 C sifted all-purpose flour
- &frac12; t salt
- 1 t baking powder
- 2 sticks butter, softened
- 2 C sugar
- 2 eggs
- 2 t vanilla

## Instructions

1. In a large bowl, sift together flour, salt, and baking powder. Set aside.
1. Using an electric mixer, cream butter and sugar until fluffy. Beat in eggs.
1. Add flour mixture, a little at a time, and mix on low speed until thoroughly combined.
1. Stir in vanilla.
1. Divide dough into thirds and flatten like a disc. Wrap dough in plastic wrap and chill for 30 minutes.
1. On a floured surface, roll dough to &frac18;" thick. Cut into desired shapes. Transfer to an ungreased cookie sheet lined with parchment paper.
1. Refrigerate uncooked cookies for 15 minutes.
1. Heat oven to 400&deg;F.
1. Bake until edges just start to brown, about 8&ndash;10 minutes. Cool on wire racks; decorate as desired.
