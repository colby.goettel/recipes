# King Arthur gingernsaps

## Ingredients

### Cookies

- &frac12;C butter (shortening can be used for hard cookies)
- 1C sugar
- &frac12;t salt
- 2t baking soda
- 1 egg
- &frac13;C molasses
- 2&frac13;C flour
- 1&ndash;2t ground ginger
- &frac12;t ground cloves
- 1t cinnamon

### Coating

- &frac14;C sugar
- 1t ground cinnamon

## Directions

- Preheat oven to 375&deg;F
- Beat butter and sugar. Add sugar, salt, and baking soda.
- Beat in egg and molasses.
- Add flour and spices.
- Combine coating ingredients.
- Make balls and add cover in coating.
- Place on baking sheet.
- Bake for 11+ minutes.
