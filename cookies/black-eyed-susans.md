# Black eyed Susans

From [Grandma Goettel](https://www.familysearch.org/tree/person/details/KWCR-R7H)

## Ingredients

- 1 C butter
- 3T sugar
- 1t vanilla
- 2C flour
- &frac12;t salt

## Instructions

1. Combine all ingredients.
1. Put tablespoon-sized balls on a baking sheet.
1. Bake at 400&deg;F for 8 minutes.
