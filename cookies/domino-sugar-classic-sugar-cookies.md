# Domino Sugar's classic sugar cookies

## Ingredients

- 4 C flour
- 2 t baking powder
- 1 t salt
- 1 &frac12; sticks butter
- 1 &frac12; C sugar
- 2 eggs
- 1 &frac12; t vanilla

## Instructions

1. Preheat oven to 350&deg;F.
1. Sift together dry ingredients.
1. Separately, cream butter and sugar.
1. Add eggs and vanilla to butter mix.
1. Once incorporated, add dry ingredients. Should form a smooth, stiff dough.
1. NOTE: You do not need to refrigerate this dough to work with it.
1. Roll out to &frac14;" thick rectangle. Cut and place on greased cookie sheet.
1. Bake at 350&deg;F for 10 minutes, or until firm and light golden brown.
