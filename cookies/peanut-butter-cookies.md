# Soft and chewy peanut butter cookies

Recipe courtesy Emily Atwood.

## Ingredients

- &frac12; C butter, softened
- 1 C peanut butter
- &frac34; C white sugar
- &frac12; C firmly packed brown sugar
- 1 egg
- 1 T milk
- 1 t vanilla extract
- &frac34; t baking soda
- &frac12; t baking powder
- &frac14; t salt
- 1&frac14; C flour

## Instructions

1. Preheat oven to 350&deg;F.
1. Beat butter and peanut butter together until well combined.
1. Add sugars and beat until fluffy.
1. Add egg, milk, and vanilla extract and mix until smooth.
1. Add baking soda, baking powder, and salt and mix just until blended.
1. Beat in flour until combined.
1. Roll balls of dough in white sugar before placing on an ungreased baking sheet.
1. Bake at 350&deg;F for 10&ndash;12&nbsp;minutes. Do not over bake; they're better softer.
1. Let cookies cool on baking sheet for at least 3&nbsp;minutes before removing to wire racks to cool completely. Store in an airtight container at room temperature.
