# Poppy seed sugar cookies (Haman's hats)

**Source:** http://www.ou.org/life/food/recipes/guide-delicious-hamantaschen-norene-gilletz/

## Ingredients

- 2 eggs
- &frac34; C sugar
- &frac12; C oil
- &frac14; C orange juice or water
- 3 C flour
- 2 t baking powder
- Additional sugar for dipping

## Instructions

1. Preheat oven to 375&deg;F.
1. Mix eggs with sugar, oil, and juice until blended.
1. Add baking powder and flour. Mix. Do not over-process.
1. Divide dough into 4 pieces. Roll each piece on a lightly floured surface into a rectangle about &# x215B;" thick. Using assorted cookie cutters, cut in different shapes. Dip each cookie lightly in sugar. Place sugar-side up on parchment-lined cookie sheets.
1. Bake at 375&deg;F for 8&ndash;10 minutes, until golden.

Variation:
- Add &frac14; C poppy seeds with dry ingredients. Roll out dough thinly, then cut in triangles using a fluted pastry wheel or pizza cutter. Dip in sugar and bake as directed. Perfect for Purim!
