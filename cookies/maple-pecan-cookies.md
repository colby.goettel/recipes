# Maple pecan cookies

**Source:** http://www.inspiredbycharm.com/2013/09/butter-pecan-maple-crisps.html

## Ingredients

- 2 C flour
- &frac12; t salt
- 1 t baking powder
- &frac14; t ground nutmeg
- 1 C butter
- 1 C sugar
- 1 t vanilla
- 1&frac12; t almond extract
- &frac12; C chopped pecans, toasted
- &frac12; C pure maple syrup

## Instructions

- Preheat oven to 300&deg;F.
- Whisk together dry ingredients.
- In a separate bowl, cream butter and sugar.
- Add vanilla and almond extract.
- Add pecans. Beat till smooth.
- On low speed, add flour mixture until a stiff dough is formed.
- Scoop onto greased cookie sheet.
- Bake at 300&deg;F for 20&ndash;25 minutes.
- Remove from oven. While hot, brush with pure maple syrup.
- Place on wire rack to cool.
