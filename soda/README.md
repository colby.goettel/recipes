# Sodas

Originally, I was making a soda syrup and force carbing in an ISI canister. However, that becomes frustrating because you can only make (and therefore consume) soda in .5 L quantities. It would be much nicer to have soda on tap and be able to control how much you have to drink.

To that end, I have recently acquired a few Corny kegs and a kegerator. Each of these recipes is being adapted to make closer to 5 gallons of soda.

## Notes

- Because I'm either kegging or using an ISI canister (for tests), all of the recipes say to force carbonate. If you can't force carbonate, use club soda in place of water.
- I live in Arizona and our tap water starts with an aftertaste. It's awful. I have a reverse osmosis filter, but it removes some of the minerals that you actually want for flavor. It's fine, but if you want a bit more flavor (and don't want to mess with water chemistry), buy gallons of spring water at the store and use them in these recipes.
- It's important to filter (even double or triple filter with increasingly fine meshes) before force carbonating. Any particulate matter will cause a bitter taste and will contribute to a shorter lifespan of the soda.
- Don't boil the juices. If you do, it'll taste more like jam than fresh soda.
