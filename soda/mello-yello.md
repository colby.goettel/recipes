# Mello Yellow clone

This recipe comes from Bucholz on the [Homebrew Talk](https://www.homebrewtalk.com/threads/mellow-yellow-clone.356357/) forum. He notes that if you want this to be more similar to Mountain Dew, substitute additional lemon and lime juice in place of the orange juice.

## Ingredients

- 2 L water
- finely grated zest and juice from 4 lemons
- finely grated zest and juice from 3 limes
- finely grated zest and juice from 6 oranges
- 3 small (2.5"&ndash;3") cinnamon sticks, broken into small pieces
- 2 T dried bitter orange peel
- 4 T dried sweet orange peel
- 2 T dried lemon peel
- 2 T whole coriander seed
- 1360 g cane sugar

Yields 3.375 gallons. TODO adjust to 5 gallons

## Instructions

- Combine the water, lemon zest, lime zest, orange zest, cinnamon, bitter orange peel, sweet orange peel, lemon peel, and coriander seeds.
- Stir in the sugar and bring to a boil, stirring until the sugar dissolves. Boil for 1 minute.
- Kill the heat and let it steep for 15 minutes.
- Cold crash.
- Double filter.
- Add juice.
- Figure out how much juice we have. This recipe feels very similar to the grapefruit soda with regards to volumes. I'm guessing that the 150/350 mL syrup/water ratio we use there will work here. Test that and then run the numbers for a full keg.
- Carbonate and serve at 30 PSI

## Notes

- The cinnamon flavor is supposedly strong for the first day or so after making, but is supposed to mellow out after that.
