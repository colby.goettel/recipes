# Notes from making knockoff Mello Yello

## Quantities

This table summarizes the amounts used for each brew, including the final yields. All measurements are in __g__ for dry ingredients and __mL__ for liquids.

| Date       | Water | Lemon juice | Lime juice | Orange juice | Bitter orange peel | Sweet orange peel | Lemon peel | Coriander seed | Syrup | Additional water |
| ---------- | ----- | ----------- | ---------- | ------------ | ------------------ | ----------------- | ---------- | -------------- | ----- | ---------------- |
| 2022-07-04 | 2000  | 150         | 50         | 700          | 13                 | 19                | 9          | 10             | 3000  | 7000             |

## 2022-07-04

- The original recipe said to do all of this in 3 gal water. I don't have a brewing system, just standard kitchen pots. It's going to be a lot easier to do this as a syrup and then mix with water later (like the grapefruit soda recipe).
- I converted the recipe to metric because that's what I'm used to using. As soon as I did, I realized that the quantities that we're working with here are exactly the same as the grapefruit recipe when doubled. Excellent. This bodes well.
- I'm using navel oranges, lemons, and limes (not key limes).
- I measured each of the dry ingredients (besides the cinnamon sticks) and noted them in the table. I figure we'll measure with a tablespoon the first few times through and then convert to grams when we're happy with the results. It'll be a better control.
- I took the cinnamon sticks, put them in a plastic bag, and smashed them with a hammer. Not super fine by any measure, just enough to get them crushed up.
- I zested the citrus into a metal bowl and then added each of the dry ingredients (minus sugar) to it.
- Got the water hot, added the sugar, and stirred and heated until it was all incorporated. Then got it up to 206&deg;F and poured it into the metal bowl.
- Stirred to incorporate, set a timer for 15 minutes, and let it steep, stirring halfway through.
- Cold crashed and filtered into another bowl. Filtered the juice in at this point. Then filtered the whole mess one more time into the (cleaned) original bowl.
- Measured total volume (3000 mL + 150 mL for prototype). Force carbonated the prototype and I'm sitting here waiting for it to carb and chill.
- Let's assume that this prototype works. Here's the math for how much water to add for 3 L of syrup:
  ```
  150   3_000
  --- = -----   =>   150x = 3_000*350   =>   150x = 1_050_000   =>   x = 7_000
  350     x  
  ```
- Wow that math is super clean. So we'll add 7_000 mL water to the 3_000 mL syrup and end up with 10 L total volume. Excellent.
- I put 6_000 mL water into the right keg
