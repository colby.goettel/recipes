# Notes from making grapefruit soda

This table summarizes the amounts used for each brew, including the final yields. All measurements are in __g__ for dry ingredients and __mL__ for liquids.

| Date       | Sugar | Citric acid | Grapefruit quant. | Juice | Syrup | Water (additional) |
| ---------- | ----- | ----------- | ----------------- | ----- | ----- | ------------------ |
| 2022-07-02 | 1360  | 42          | 10&sup1;          | 2200  | 4000  | 8000 in keg        |
| 2022-07-09 | 1361  | 42          | 8 ruby red        | 2100  | 3900  | 9100               |

## 2022-07-02

- &sup1;I bought four pink grapefruit and six California ruby red grapefruit. I didn't know there was a difference between the two, but now that I think about it, I've always done this with pink grapefruit before. I'm not too worried about the taste being different because I hate pink grapefruit juice just as much as I hate ruby red grapefruit juice, so with the syrup it shouldn't make a difference.
- I tried to keep all of the pulp that I could. My thinking is that I will filter all of the pulp and rind at the same time (post 24 hours in the fridge).
- I didn't adjust the amount of sugar or water for the increased amount of juice (from the original recipe) because I've made this before with this ratio of juice and really enjoyed it, so
- I let the hot syrup (water, sugar, citric acid, zest) hang out on the counter for a few hours while I ran some errands. When I got home, it had cooled from 190&deg;F to 91&deg;F. I figured it was time to get it under 80&deg;F so I could add the juice and throw it in the fridge. Did a water and ice bath and stirred it around and dropped the temperature in a couple minutes.
- 2022-07-02T22:30 Combined with the juice, covered, put in the fridge.
- Next steps: filter, filter even finer (really wondering how long this fine mesh coffee filter I picked up is gonna take; I used a cloth last time and it took for freaking ever), measure resulting volume, try 3:1 ratio in ISI, go from there.
  - I double filtered through a fine mesh sieve. It will take approximately nine years to filter through the coffee filter so I gave up on that notion. Pretty sure the towel is faster, but I don't care at this point.
- 2022-07-03T19:00 No joke. I put aside 150 mL for prototyping and ended up with exactly 4000 mL of syrup remaining. 4 L of syrup is absolutely insane.
- A note on the 150 mL. As soon as I grabbed my 500 mL Pyrex (what I have historically used to measure for the ISI), I remembered that there is no mark at 125 mL, just 100 mL and 150 mL, so I've always done 150 mL syrup plus the remainder as water. So, for prototype 1, I am doing 150 mL syrup plus 350 mL water.
  - Let's assume that this prototype works. Here's the math for how much water to add for 4 L of syrup:
  ```
  150   4000
  --- = ----   =>   150x = 4000*350   =>   150x = 1_400_000   =>   x = 9333 1/3
  350    x  
  ```
  - This totally works! Yes! 9,350 mL water (easier measuring) plus 4,000 mL syrup gives us 13,350 mL total volume which is less than the 18,927 mL maximum limit.
  - Since I already have 8,000 mL water in the keg chilling, I only need to add an additional 1,350 mL water.
- Prototype results: I absolutely hate the flavor of ruby red grapefruit. I think it would be best to throw this out and remake it with 8 pink grapefruit (same juice yield). But, more importantly, the 150/350 mL mix works perfectly. Great amount of sweetness and it tastes half fine and half awful. Shucks.

## 2022-07-09

- I used 8 star ruby grapefruit. I think these are the kind I've used previously. Would really like to try this with pink grapefruit.
- Steeped for 15 minutes, cold crashed to room temperature, filtered in juice, filtered whole, and then filtered through kitchen cloth. I'm really trying to control the amount of dregs in this batch. The Mello Yello I made had particulate cinnamon which is less than desirable.
- Let's assume that this prototype works. Here's the math for how much water to add for 4 L of syrup:
```
150   3_900
--- = -----   =>   150x = 3_900*350   =>   150x = 1_365_000   =>   x = 9_100
350     x  
```