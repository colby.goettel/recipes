# Grapefruit soda

This recipe comes from [Glen and Friends Cooking](https://www.youtube.com/watch?v=xJc_q1ZNv48). I've doubled it so it's closer to the amount used to make a keg.

## Ingredients

- 2 L water
- 13600 g cane sugar
- 42 g citric acid
- 8 pink or star ruby grapefruit

## Instructions

### Syrup

- In a large pot, heat water and add sugar and citric acid.
- Once combined, add to grapefruit zest and allow to steep for 15 minutes.
- Cold crash the syrup.
- Filter in the juice. Filter again. Filter a third time through a cloth to remove particulate matter.

### Force carbonating

The rough proportion is 150 mL syrup combined with 350 mL water. That amount fits perfectly in an ISI canister. If you're happy with that result, run the proportion for the amount of syrup you have, add to a keg, and carbonate and serve at 30 PSI.

## Notes

- In the original recipe, Glen says you should get 400&ndash;500 mL of juice from four grapefruit. Four pink grapefruit gives me around 1050 mL of juice and four California ruby red grapefruit gives me 770 mL of juice. I use an electric countertop juicer which may yield more juice? Not sure. But even if it does, there's no way it's giving over double what Glen was getting. Was he not using all of his juice?
- Absolutely hate the taste of ruby red grapefruit. Threw out the batch that I went halfsies on grapefruit. Never using those again. If you like it, go nuts; it's your soda.
- I haven't played around with zest and juice quantities to tell you if there's some perfect combination out there. I've always just used everything I had. This is just the information I have at this time (2022-07-02).
- Also in the original recipe, Glen says to use the zest of three grapefruit, but the juice of four. I've always used the zest and juice of all four.
