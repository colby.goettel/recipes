# Cream soda

This recipe comes from [Glen and Friends Cooking](https://www.youtube.com/watch?v=NOhnrLhyOtU).

## Ingredients

- 2 L water
- 650 g cane sugar
- 100 g lactose (optional; helps with the head)
- 5 g cacao nibs
- 1 g (&frac14; t) citric acid
- pinch of salt
- 100 mL vanilla extract

## Method

- Heat the water until almost boiling
- Mix in the sugar, lactose, cacao, citric acid, and salt
- Stir in the water until the sugar dissolves
- Cover and set aside to cool
- Once cool, strain out the cacao and mix in the vanilla
- Mix 1 part syrup with 2 parts water and force carbonate

## For 5 gal (18.925 L) keg

- Original recipe is about 2.25 L of syrup. Mix 2.25 L syrup + (2*2.25 L) water should yield 6.75 L soda. 18.925 L / 6.75 L = 2.8x recipe for 1 keg, assuming initial starting syrup and ratio are correct. Which really means that the original recipe would be perfect in a half Corny keg.
