# Recipes

I have tried to be explicit in these recipes, but something I've been doing something the same way for so long that I forget that it's even worth mentioning. Or, some things (to me) are a given so I omit them from the recipe. Here are some guidelines in case you get lost:

- When cutting vegetables, mince is super fine (&lt;&frac14;"), dice is fine (like &frac14;&ndash;&frac12;"), and chopped is large (&gt;&frac12;")
- I always use salted butter.
- It's always large eggs.
- For non-ground beef, I always season my beef and let it rest before cooking. If I'm slow cooking (e.g., crock pot, braising), I always sear it first.
- For ground beef, I season with Lawry's seasoned salt and fresh cracked pepper.
- Unless otherwise specified:
  - "flour" means "all-purpose flour"
  - "sugar" means "white, granulated cane sugar"

## Appetizers

- [Cheeseball](appetizers/cheeseball.md)
- [Hot artichoke dip](appetizers/hot-artichoke-dip.md)

## Breads

- [Almond poppy seed muffins](bread/almond-poppy-seed-muffins.md)
- [Australian damper bread](bread/australian-damper-bread.md)
- [Banana bread](bread/banana-bread.md)
- [Beer bread](bread/beer-bread.md)
- [Biscuits](bread/biscuits.md)
- [Cinnamon rolls](bread/cinnamon-rolls.md)
- [French bread](bread/french-bread.md)
- [Paul Peterson's crunchy bread](bread/paul-petersons-crunchy-bread.md)
- [Russian hot bread](bread/russian-hot-bread.md)
- [Swiss bread](bread/swiss-bread.md)
- [Unleavened bread for Passover](bread/unleavened-bread-for-passover.md)

## Breakfast

- [Bacon cornmeal pancakes](breakfast/bacon-cornmeal-pancakes.md)
- [Dutch oven egg souffl&eacute;](breakfast/dutch-oven-egg-souffle.md)
- [German pancakes (Dutch baby pancake)](breakfast/german-pancakes.md)
- [Granola](breakfast/granola.md)
- [Pancakes](breakfast/pancakes.md)
- [Potato cakes](breakfast/potato-cakes.md)
- [Waffles](breakfast/waffles.md)
- [Whole wheat pancakes](breakfast/whole-wheat-pancakes.md)

## Cakes

- [Blueberry cheesecake](dessert/blueberry-cheesecake.md)
- [Chocolate cake](cake/chocolate-cake.md)
- [Chocolate mousse cake](dessert/chocolate-mousse-cake.md)
- [Dr. Pepper chocolate cake](cake/dr-pepper-chocolate-cake.md)
- [Jewish coffee cake](cake/jewish-coffee-cake.md)
- [Lemon cake](cake/lemon-cake.md)
- [Poppy seed cake](cake/poppy-seed-cake.md)
- [Texas sheet cake](cakes/texas-sheet-cake.md)

## Cookies

- [Black eyed Susans](cookies/black-eyed-susans.md)
- [Domino Sugar's Classic sugar cookies](cookies/domino-sugar-classic-sugar-cookies.md)
- [Gingersnaps](cookies/gingersnaps.md)
- [Guittard chocolate chip cookies](cookies/guittard-chocolate-chip-cookies.md)
- [Haman's hats (poppy seed sugar cookies)](cookies/hamans-hats.md)
- [Jan's cookies](cookies/jans-cookies.md)
- [Maple pecan cookies](cookies/maple-pecan-cookies.md)
- [Martha Stewart's sugar cookies](cookies/martha-stewarts-sugar-cookies.md)
- [Molasses cookies](cookies/molasses-cookies.md)
- [Oatmeal cookies](cookies/oatmeal-cookies.md)
- [Oatmeal, walnut, chocolate chip cookies](cookies/oatmeal-walnut-chocolate-chip-cookies.md)
- [Peanut butter cookies](cookies/peanut-butter-cookies.md)
- [Snickerdoodles](cookies/snickerdoodles.md)

## Dessert

- [Crumb topping](dessert/crumb-topping.md)
- [Divinity](dessert/divinity.md)
- [Hawaiian butter mochi (mochi blondies)](dessert/hawaiian-butter-mochi.md)
- [Kay's fudge](dessert/kays-fudge.md)
- [Mom's apple pie filling](dessert/moms-apple-pie-filling.md)
- [Nick Estrada's apple pie](dessert/nick-apple-pie.md)
- [Pie crust](dessert/pie-crust.md)
- [See's fudge](dessert/sees-fudge.md)

## Dressings

- [Coleslaw dressing](dressings/coleslaw-dressing.md)
- [Honey French dressing](dressings/honey-french-dressing.md)

## Drinks

- [Egg nog](drinks/egg-nog.md)
- [Iced tea](drinks/iced-tea.md)
- [Japanese iced coffee](drinks/japanese-iced-coffee.md)
- [Slush](drinks/slush.md)
- [Sun tea](drinks/sun-tea.md)

## Entrées

- [Beef and Guinness stew](entrees/beef-and-guinness-stew.md)
- [Birria de res](entrees/birria-de-res.md)
- [Broccoli and cheese soup](entrees/broccoli-and-cheese-soup.md)
- [Butter chicken](butter-chicken.md)
- [Chicken wings](entrees/chicken-wings.md)
- [Chuck roast](entrees/chuck-roast.md)
- [Grandma Goettel's enchilada sauce](entrees/grandma-goettels-enchilada-sauce.md)
- [Grant's burros](entrees/grants-burros.md)
- [Knockoff Ron's chili](entrees/knockoff-rons-chili.md)
- [Korean BBQ chicken](entrees/korean-bbq-chicken.md)
- [Kun Koki steak](entrees/kun-koki-steak.md)
- [Lasagna](entrees/lasagna.md)
- [Masa](entrees/masa.md)
- [Meatball subs](entrees/meatball-subs.md)
- [Meatballs](entrees/meatballs.md)
- [Meatloaf](entrees/meatloaf.md)
- [Mom's lasagna](entrees/moms-lasagna.md)
- [Pasta with shrimp or chicken](entrees/pasta-with-shrimp.md)
- [Quesabirria](entrees/quesabirria.md)
- [Salsa alle noci (walnut pesto)](entrees/salsa-alle-noci.md)
- [Soupe Dauphinoise](entrees/soupe-dauphinoise.md)
- [Sweet potato pie](entrees/sweet-potato-pie.md)
- [Yorkshire pudding](entrees/yorkshire-pudding.md)

## Household

- [Upholstery cleaner](household/upholstery-cleaner.md)

## Salsa

- [Beverly Baldwin's gringo salsa](salsa/beverly-baldwins-gringo-salsa.md)
- [Green mayo](salsa/green-mayo.md)
- [Habanero sauce](salsa/habanero-sauce.md)
- [J.R.'s salsa](salsa/jrs-salsa.md)
- [Los Hermano's copycat salsa](salsa/los-hermanos.md)
- [Salsa birria](salsa/salsa-birria.md)
- [Salsa taqueria](salsa/salsa-taqueria.md)
- [Uncle Bryan's green salsa](salsa/uncle-bryans-green-salsa.md)

## Sauces

- [Creamy horseradish](sauces/creamy-horseradish.md)
- [Orange sauce](sauces/orange-sauce.md)
- [Red sauce](sauces/red-sauce.md)
- [Tempura dipping sauce](sauces/tempura-dipping-sauce.md)
- [Thai peanut sauce](sauces/thai-peanut.md)

## Sides

- [Dr. Hunn's pork and beans](sides/pork-and-beans.md)

## Soda

[Notes on soda making, techniques, etc.](soda/README.md) Further notes can be found in [brew-day-notes/](soda/brew-day-notes/).

- [Cream soda](soda/cream-soda.md)
- [Grapefruit](soda/grapefruit.md)
- [Mello Yello (knockoff)](soda/mello-yello.md)
