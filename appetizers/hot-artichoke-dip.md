# Hot artichoke dip

## Ingredients

- 1 C Parmesan cheese, grated
- &frac12; C mayonnaise
- &frac12; C sour cream
- 1 can artichoke hearts in water, drained, chopped
- 1 t garlic
- 1&frac12; t dill weed

## Instructions

1. Preheat oven to 350&deg;F.
1. Mix everything together.
1. Pour into a small, greased casserole dish (9"x9" works).
1. Bake at 350&deg;F for 30 minutes or until the top is golden brown.
