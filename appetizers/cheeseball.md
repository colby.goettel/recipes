# Cheeseball

## Ingredients

- 12 oz cream cheese, softened
- &frac14; C mayonnaise
- 1 t dill weed
- &frac14; t pepper
- 1 bunch green onions
- &frac14; C+ bacon pieces (optional)
- Chopped pecans

Yields 3 four inch cheeseballs.

## Instructions

1. Cream together the cream cheese, mayonnaise and seasonings until smooth. Blend in sliced green onions and bacon.
1. Form balls of desired size. Place on plate and tarp with saran wrap. Refrigerate till firm. Reshape and roll bolls in a bowl of chopped pecans.
