# Salsa birria

- 8&ndash;10 chile de arbol
- 2 chile pasilla
- 10&ndash;12 tomatillos
- 2&ndash;3 cloves garlic
- &frac18;&ndash;&frac14; white onion
- Salt and pepper to taste

## Instructions

1. In a heavy-bottom pot, add 1&ndash;2 L water (measure and report back) and bring to a boil.
1. Add chiles and tomatillos and boil for 13 minutes.
1. Kill the heat and let it steep for 10 minutes.
1. Transfer to a blender and add a ladleful of the steeped liquid and blend until smooth. Add more water as necessary to reach desired consistency. Remember that it will thicken slightly as it cools.
