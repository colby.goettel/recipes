# Los Hermanos salsa

## Ingredients

- 3 cans 28oz diced tomatoes
- 1 jalapeno
- 1 Anaheim
- &frac13; bunch of cilantro
- 3 or 4 green onions
- 1T salt
- 2 cloves garlic

## Directions

- Drain tomatoes
- Blend (might need to do two loads depending on blender capacity)
