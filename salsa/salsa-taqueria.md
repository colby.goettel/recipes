# Salsa taqueria

## Ingredients

- like 11 tomatillos
- two cloves garlic (she said one, but come on)
- two dried chile california
- half cup+ dried, roasted chile arbol (can buy roasted or roast yourself)

## Directions

- roast tomatillos. roast, flip, roast. Once roasted, add two chile california to the bottom of the pan (lots of water is going to come off this), turn off heat, cover, let cool. About ten minutes.
- blend/food processor chile arbol and garlic. you want a fine powder. The color comes from the chile california.
- Once tomatillos and chile california are cooled, blend/process 3/4 with arbol/garlic mixture. No need to add water because the tomatillos have enough. Season with salt. Rough blend in the rest of the tomatillos. This should stop the entire mixture from becoming gummy.
