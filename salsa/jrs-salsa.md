# J.R.'s salsa

## Ingredients

- 4 28 oz cans crushed tomatoes
- 3 cans diced jalapeños
- 4 habanero blended with 12 oz water
- 4-6 serrano
- 1 lb white onion
- 1 bundle cilantro
- 12 oz water
- 2 T salt
- 1 T garlic powder
- 1 T oregano
