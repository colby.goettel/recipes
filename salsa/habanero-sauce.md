# Habanero Sauce

I originally got this from Javier Maya in Regina, SK, but have adapted it and changed it into my own thing.

## Ingredients

- 12&ndash;13 habaneros (preferably green, but orange are fine)
- 4 cloves garlic
- 1 Roma tomato
- 1 t salt
- &frac12; C white vinegar (enough to blend)

## Directions

- Remove stems from habaneros.
- Add everything except vinegar. Add enough vinegar to blend. I don't know exaclty how much cause I don't measure this part, but it feels like &frac14;&ndash;&frac12;&nbsp;C.
- If the sauce is too spicy, add another tomato. If it's still too spicy, eat something else.
