# Poppy seed cake

From [Aunt Kay](https://www.familysearch.org/tree/person/details/K26F-HCV)

## Ingredients

- Yellow cake mix
- 1pkg lemon instant pudding
- 4 eggs
- &frac14;C poppy seeds
- &frac34;C oil
- 1C water

## Instructions

1. Mix all for 3 minutes.
1. Pour into greased bundt cake pan or two loaf pans.
1. Bake at 350&deg;F for 40&ndash;45 minutes.
