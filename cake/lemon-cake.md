# Lemon cake

## Ingredients

- 1 lemon cake mix
- 1 lemon instant pudding mix
- 1 C sour cream
- &frac12; C oil
- &frac12; C water
- 4 eggs
- 1 lemon, juiced

### Frosting

- Zest of 1 lemon
- 1 container of lemon flavored frosting

## Instructions

1. Preheat oven to 350&deg;F.
1. Zest the lemon and set aside.
1. Mix everything together.
1. Pour into greased bundt cake pan.
1. Bake at 350&deg;F for 50&ndash;60 minutes or until a toothpick comes out clean.
1. Once cooled, add lemon zest to frosting and coat cake.
