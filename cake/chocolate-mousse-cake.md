# Chocolate mousse cake

From [Grandma Goettel](https://www.familysearch.org/tree/person/details/KWCR-R7H)

## First layer

- 1 C flour
- &frac12;C nuts
- 1 cube margarine
- &frac12;C brown sugar

1. Mix together and press in bottom of a 9x13 pan
1. Bake at 350&deg;F for 15 minutes or until golden brown
1. Allow to cool

## Second layer

- 1C powdered sugar
- 1 8oz package cream cheese
- 1C cool whip

1. Mix with electric mixer until smooth
1. Spread over cooled first layer

## Third layer

- 1 small package instant vanilla pudding
- 1 small package instant chocolate pudding
- 3C milk

1. Mix together
1. Pour over cream cheese mixture
1. Top with cool whip
