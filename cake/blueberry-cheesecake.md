# Blueberry cheesecake

From [Grandma Goettel](https://www.familysearch.org/tree/person/details/KWCR-R7H)

## Ingredients

### Crust

- 2 C graham cracker crumbs
- &frac12;C butter
- &frac12;C sugar

### Filling

- 2 8oz packages cream cheese
- 2 eggs
- 1C sugar
- 1t vanilla

### Topping

- 1 can blueberry pie filling

## Instructions

1. Form crust together and place in a 9x13 pan
1. Mix filling ingredients together until smooth
1. Pour over crust
1. Bake at 350&deg;F for 15 minutes. Don't over cook.
1. Let cool and then pour can  of blueberry pie filling over top.
