# Ann Myer's chocolate cake

## Ingredients

- 1 chocolate cake mix
- 1 chocolate instant pudding mix
- 1 C sour cream
- &frac12; C oil
- &frac12; C water
- 4 eggs
- Chocolate chips

## Instructions

1. Preheat oven to 350&deg;F.
1. Mix everything together.
1. Fold in chocolate chips. Whatever you're comfortable with.
1. Pour into greased bundt cake pan.
1. Bake at 350&deg;F for 50&ndash;60 minutes, or until a toothpick comes out clean.
