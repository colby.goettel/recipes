# Jewish coffee cake

## Ingredients

- &frac12; C butter
- 1 C sugar
- 2 eggs
- 2 C flour
- 1 t baking soda
- 1 t baking powder
- 1 C sour cream
- 1 t vanilla
- &frac12; C powdered sugar
- 2 t cinnamon
- 2 T butter

## Instructions

1. Preheat oven to 350&deg;F.
1. Cream butter, sugar, and eggs.
1. Combine flour, baking soda, and baking powder. Add to creamed butter mixture.
1. Add sour cream and vanilla.
1. Separately, mix powdered sugar, cinnamon, and butter.
1. Marble cinnamon mixture in with the rest of the batter.
1. Bake at 350&deg;F for an hour or until done.
