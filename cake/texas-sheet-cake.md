# Texas sheet cake

From [Grandma Goettel](https://www.familysearch.org/tree/person/details/KWCR-R7H)

## Ingredients

- 2C sugar
- 2C flour
- &frac12;t salt
- 1C butter
- 1C water
- 4T cocoa powder
- &frac12;C sour cream (or &frac12;C canned milk and &frac12;t white vinegar)
- 2 eggs
- 1t baking soda
- Chocolate frosting

## Instructions

1. In a large bowl, mix together the sugar, flour, and salt.
1. In a small pan, add butter, water, and cocoa powder and bring to a boil. Add to the dry ingredients.
1. Add sour cream (or milk and vinegar), eggs, and baking soda.
1. Beat with electric mixer until smooth.
1. Pour into a greased and floured cookie sheet and bake at 375&deg;F for 20&ndash;25 minutes.
1. Frost while still warm.
