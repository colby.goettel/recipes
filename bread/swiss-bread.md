# Swiss bread

## Ingredients

- 1 T yeast
- &frac34; C water
- &frac34; C milk
- &frac14; C shortening
- &frac14; C sugar
- 1&frac12; t salt
- 1 egg
- 3&ndash;4 C flour

## Instructions

1. Dissolve yeast in warm water with a pinch of sugar. Allow to rest for 10 minutes to activate.
1. In a saucepan over medium heat, warm milk, shortening, sugar, and salt. Do not let the milk boil.
1. Allow milk mixture to cool. This is important or you will burn your hand while kneading.
1. Once cooled, combine with yeast mixture.
1. Add beaten egg and 2 C flour. Beat and continue to add flour until dough is finished. Should be a little sticky.
1. Allow time to rise. Divide into three sections and roll out like bread sticks. Braid. Let rise.
1. Bake at 375&deg;F for 30&ndash;35 minutes.

## Notes

- I haven't made this in forever. Is it really necessary to scald the milk? Try it both ways and see.
