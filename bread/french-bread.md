# French bread

## Ingredients

- 1 T yeast
- 2 C warm water
- 1 T sugar
- 2 t salt
- 1 T vegetable oil
- 5&ndash;5&frac12; C flour
- Cornmeal

## Instructions

1. Dissolve yeast and a pinch of sugar in warm water. Allow yeast to activate (about 10 minutes).
1. Add salt, oil, and 3 C flour; beat for 2 minutes.
1. Stir in 2 C flour to make a stiff dough.
1. Knead until smooth and elastic, about 10 minutes.
1. Place in oiled bowl, turn dough to coat all sides, cover, and let rise until doubled in size.
1. Punch down and divide in half.
1. Shape dough into two long slender loaves.
1. Grease a large cookie sheet and sprinkle with cornmeal.
1. Place loaves in pan and cut diagonal slashes on top of each loaf.
1. Cover and let rise until doubled.
1. Bake at 375&deg;F for about 30 minutes.

## Notes

- You can sprinkle or spray water on the loaves during baking if you want a really crunchy crust. Make sure you don't spray the oven window or it can shatter.
