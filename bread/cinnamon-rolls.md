# Cinnamon rolls

## Ingredients

- 1.5 T yeast
- 1 C lukewarm water
- 1 t sugar
- 8 C flour
- 1 C sugar
- 6 T lard
- 2 C water
- 2 eggs, beaten
- 1 T salt

### Filling

- &frac12; C softened butter
- 1&frac12; C brown sugar
- 2 T cinnamon

### Frosting

- 4 T butter
- 2 C powdered sugar
- 4 T milk
- 1 t vanilla extract

## Instructions

1. Start the yeast in a cup of lukewarm water with a teaspoon of sugar.
1. Cut the lard into the dry ingredients.
1. Combine beaten eggs, water, and yeast starter. Add half the flour and make a batter. Keep adding flour until the dough is formed. It's okay if it's a little sticky. Knead until the dough is smooth.
1. Split the dough in half. For each half:
  1. Roll out half the dough.
  1. Mix the filling. Spread it out onto the dough.
  1. Roll, cut into segments, put into a greased 9"x13" pan.
1. Cover and let rise.
1. Heat oven to 350&deg;F.
1. Bake for 20&ndash;30min, or until tops are golden brown.
1. Once cooled, make the frosting and evenly apply to the baked rolls.
