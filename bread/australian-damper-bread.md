# Australian damper bread

## Ingredients

- 4 C flour
- 1 t salt
- &frac18; C baking powder
- &frac18; C sugar
- 1 &frac14; C milk

## Instructions

1. Preheat oven to 425&deg;F.
1. Grease an 8"x8" pan.
1. Whisk the flour, salt, baking powder, and sugar together.
1. Add milk and stir until a stiff dough forms.
1. Press into the pan. Score the dough with a sharp knife, making a grid pattern.
1. Bake at 425&deg;F for 20-25 minutes, or until golden brown. Serve immediately.
