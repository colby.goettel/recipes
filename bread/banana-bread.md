# Banana bread

## Ingredients

- &frac12; C butter
- &frac34; C brown sugar
- &frac14; t salt
- 1 t baking soda
- 2 eggs
- 6 bananas, mashed
- 2 C flour
- (optional) chocolate chips
- (optional) walnuts

## Instructions

1. Preheat oven to 350&deg;F.
1. In a large bowl, cream butter and sugar.
1. Add eggs and beat.
1. Mix in baking soda and salt.
1. Incorporate mashed bananas.
1. Finally, add flour and beat until smooth.
1. (optional) Add as many chocolate chips or walnuts as you think is reasonable. Dark chocolate works best.
1. Pour batter into greased loaf pans. This recipe will make one large standard 9"x5" loaf, or multiple mini loaves.
1. Bake for 50&ndash;60 minutes for a large loaf (less time for smaller loaves), or until a toothpick inserted into the center comes out clean.
