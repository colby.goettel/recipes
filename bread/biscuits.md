# Biscuits

## Ingredients

- 2 C flour
- 1 T baking powder
- 1 t salt
- &frac14; C shortening or lard
- &frac34; C milk

Yields a dozen biscuits.

## Instructions

1. Preheat oven to 450&deg;F.
1. Sift dry ingredients together.
1. Using a pastry knife, cut in shortening or lard.
1. Stir in milk. Don't over knead.
1. Roll out to &frac12;" thick.
1. Use a cup to cut out biscuits.
1. Bake at 450&deg;F for 10-12 minutes.
