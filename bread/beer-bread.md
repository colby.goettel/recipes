# Beer bread

## Ingredients

- 3 C floud
- 3 t baking powder (omit if using self-rising flour)
- 1 t salt (omit if using self-rising flour)
- &frac14; C sugar
- 1 12oz can beer
- &frac14; C butter, melted

## Instructions

1. Preheat oven to 375&deg;F
1. Mix dry ingredients together.
1. Pour in beer and mix to form a dough.
1. Put in a loaf pan and pour melted butter on top.
1. Bake 50&ndash;60 minutes.
1. Remove from pan and allow to cool for at least 15&nbsp;minutes.

## Notes

- I've always done this with a golden ale or Fat Tire. Anything like that works great. You don't get the hops bitterness passed through. Probably worth experimenting with an IPA if you're of that persuasion. Any golden lager or ale should do just fine, I just keep Fat Tire stocked and it's an affordable option for this.
