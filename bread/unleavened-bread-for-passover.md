# Unleavened bread for Passover

## Ingredients

- 4 C flour
- 1&frac12; C water
- 2 T olive oil

## Instructions

1. Preheat oven to 450&deg;F.
1. Combine ingredients and knead for ten minutes.
1. Roll into a ball, cut in half, then cut each half into 8 pieces.
1. Roll out each piece as thinly as possible into ovals.
1. Pierce with a fork several times to eliminate air holes.
1. Place on baking sheet covered with parchment paper.
1. Bake at 450&deg;F for about 5 minutes, or until slightly browned.
