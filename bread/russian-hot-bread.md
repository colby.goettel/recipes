# Russian hot bread

## Ingredients

- 2&frac12; t yeast
- &frac14; C sugar
- 1 C warm water
- 1 egg
- 6 T butter
- 1 T salt
- 3 C flour

## Instructions

1. Mix yeast, sugar, and warm water. Let sit to activate.
1. Separately, mix egg, butter, and salt. Slowly add flour until combined.
1. Knead.
1. Let rise.
1. Bake at 450&deg;F for ???.

## Notes

- Works great as pizza dough.
