# Almond poppy seed muffins

I have adapted this recipe from https://www.yourcupofcake.com/better-than-costco-almond-poppy-seed-muffins/.

## Ingredients

- 3 C flour
- 1 C sugar
- 2 T poppy seeds
- 1 T baking powder
- &frac12; t baking soda
- &frac12; t salt
- 1 &frac12; C sour cream or plain yogurt
- &frac14; C milk
- 1 t vanilla extract
- 2 t almond extract
- 2 eggs
- &frac12; C butter, melted and cooled slightly

Yields 12&ndash;14 muffins.

## Instructions

1. Preheat oven to 400&deg;F.
1. Combine sour cream, milk, vanilla extract, almond extract, eggs, and melted butter.
1. In a separate, large bowl, combine flour, sugar, poppy seeds, baking powder, baking soda, and salt.
1. Add wet ingredients to dry ingredients and combine. This is an incredibly thick batter.
1. If you want tall muffins, pour batter into greased muffin tins until full or near full. Those muffins will have wide muffin tops. For more manageable muffin tops, fill the muffin tins three quarters full.
1. Place in oven and lower temperature to 375&deg;F. Bake 18&ndash;22 minutes.
