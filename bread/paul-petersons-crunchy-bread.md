# Paul Peterson's crunchy bread

## Ingredients

- 1&frac12; t yeast
- 8 C flour
- 4 t salt
- 720 g water

## Instructions

1. Add yeast, some warm water, and a bit of sugar in a bowl. Let rest for ten minutes to activate.
1. Combine flour, salt, yeast mixture, and remainder of 720 g water to bowl.
1. Knead until smooth.
1. Let rise.
1. Split into two loaves.
1. Bake in a cloche or Dutch oven at 480&deg;F for 15&ndash;20 minutes, or until golden brown.
