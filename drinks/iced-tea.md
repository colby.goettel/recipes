# Iced tea

## Ingredients

- 1 t loose leaf tea for every cup of water
- Water

## Instructions

1. For a 2 L batch, use 8.5 t of loose leaf tea.
1. Put a lid on the container and put in the fridge overnight or for at least 12 hours.
1. Strain the liquid through a fine mesh sieve into another 2 L jug.
1. Ice and enjoy.
