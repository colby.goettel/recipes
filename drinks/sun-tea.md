# Sun tea

## Ingredients

- 2 L water
- 9? sachets tea (see notes)

## Instructions

1. Fill a glass jug with 2 L of water.
1. Add the tea to the water.
1. Put a loose lid on the container.
1. Put the container in direct sunlight for two hours.
1. Move the container to the shade for a few hours.
1. Bring inside, chill, ice, and enjoy.

## Notes

- I always use Harney & Sons Paris tea which is a delightful Earl Grey with blackcurrant. It comes in sachets which is a double tea bag. Adjust the recipe accordingly if you're using normal tea bags or measuring out loose leaf tea.
