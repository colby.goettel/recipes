# Dr. Hunn's pork and beans

From [Bob Hunn](https://www.familysearch.org/tree/person/details/LJQ6-CGT)

## Ingredients

- 2 21oz cans of pork and beans (we're doctoring them up)
- &frac12;C brown sugar
- &frac12;C ketchup
- 1t dry mustard
- 4&ndash;6 slices bacon, slightly browned
- 1 medium onion, diced
- 2T dark molasses

## Instructions

1. Drain juice from beans, and add all ingredients to a pot.
1. Cover and bake at 350&deg;F for 20 minutes. Uncover and bake for an additional 20 minutes.
